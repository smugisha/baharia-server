package lib

import (
	"encoding/xml"
	"fmt"
	"html"
	"log"
	"net/http"
	"time"

	"github.com/advancedlogic/GoOse"
	"github.com/dpup/gohubbub"
	"github.com/kennygrant/sanitize"

	"bitbucket.org/smugisha/baharia-server/core/config"
	"bitbucket.org/smugisha/baharia-server/core/model"
	"bitbucket.org/smugisha/baharia-server/core/repos"
)

var (
	HubClient *gohubbub.Client
	g         = goose.New()
)

type Feed struct {
	Status  string  `xml:"status>http"`
	Entries []Entry `xml:"entry"`
}

type Entry struct {
	Links []Link `xml:"link"`
	Title string `xml:"title"`

	Description string `xml:"description"`
	Content     string `xml:"content"`

	Author Author `xml:"author"`

	Thumbnail Thumbnail `xml:"thumbnail"`
}

type Link struct {
	Rel   string `xml:"rel,attr"`
	Title string `xml:"title,attr"`
	Href  string `xml:"href,attr"`
	Type  string `xml:"type,attr"`
}

type Author struct {
	Name string `xml:"name"`
}

type Thumbnail struct {
	Url    string `xml:"url,attr"`
	Height string `xml:"height,attr"`
	Width  string `xml:"width,attr"`
}

func NewHubClient(conf *config.Config) {
	HubClient = gohubbub.NewClient(
		fmt.Sprintf("%s%s", "74.74.175.42", conf.Hub.Port), conf.Hub.Watcher)

}

func StartHub(mux *http.ServeMux) {
	topics, err := repos.Topics.FindAll()

	if err != nil {
		log.Printf("Error fetching %v", err)
	}

	for _, topic := range topics {
		HubClient.DiscoverAndSubscribe(topic.Link, HandleEntry)
	}

	HubClient.RegisterHandler(mux)

	go HubClient.Start()
}

func HandleEntry(contentType string, body []byte) {
	var feed Feed
	xmlError := xml.Unmarshal(body, &feed)

	if xmlError != nil {
		log.Printf("XML Parse Error %v\n", xmlError)
	} else {
		var contentHtml string

		for _, entry := range feed.Entries {
			article := model.NewArticle(entry.Title, entry.Author.Name, time.Now().String())
			article.Thumbnail = model.Thumbnail{
				Url:    entry.Thumbnail.Url,
				Height: entry.Thumbnail.Height,
				Width:  entry.Thumbnail.Width,
			}

			if entry.Content != "" {
				article.Content = entry.Content
			} else {
				article.Content = entry.Description
			}

			contentHtml = html.UnescapeString(article.Content)
			content, err := g.ExtractFromRawHTML(entry.Links[0].Href, contentHtml)
			if err != nil {
				log.Fatal(err)
			} else {
				article.Content = content.CleanedText
				article.TopImage = model.Thumbnail{
					Url:    content.TopImage,
					Height: "0",
					Width:  "0",
				}
			}

			article.Intro = sanitize.HTML(contentHtml)

			for _, link := range entry.Links {
				if link.Rel == "alternative" || link.Type == "text/html" {
					article.Link = link.Href
				}
			}

			repos.Articles.Store(article)
		}
	}
}

func UnsubscribeAll(c *gohubbub.Client) {
	topics, err := repos.Topics.FindAll()

	if err != nil {
		log.Printf("Error fetching %v", err)
	}

	for _, topic := range topics {
		c.Unsubscribe(topic.Link)
	}
}
