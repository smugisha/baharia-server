package lib

import (
	"log"
	"net/http"
)

func TwitterOauthEcho(r *http.Request) bool {
	var res bool

	provider := r.Header["X-Auth-Service-Provider"]
	auth := r.Header["X-Verify-Credentials-Authorization"]

	if provider == nil || auth == nil {
		return false
	}

	req, err := http.NewRequest("GET", provider[0], nil)
	if err != nil {
		log.Printf("Error: %v\n", err)
	}

	req.Header.Add("Authorization", auth[0])

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error: %#v\n", err)
	}

	if resp.StatusCode == 200 {
		res = true
	} else {
		res = false
	}

	return res
}
