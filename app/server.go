package app

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/smugisha/baharia-server/app/controllers"
	"bitbucket.org/smugisha/baharia-server/app/lib"
	"bitbucket.org/smugisha/baharia-server/core/config"
)

func Server(conf *config.Config) {
	lib.NewHubClient(conf)

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome to home page!")
	})

	// topics or feed sources
	mux.HandleFunc("/add/topic", controllers.AddTopic)

	// entries or articles
	mux.HandleFunc("/articles", controllers.Articles)
	mux.HandleFunc("/article", controllers.Article)
	mux.HandleFunc("/article/like", controllers.Like)
	mux.HandleFunc("/article/dislike", controllers.Dislike)

	// users
	mux.HandleFunc("/user/signup/digits", controllers.TwitterRegistration)

	go func() {
		lib.StartHub(mux)
	}()

	s := &http.Server{
		Addr:           conf.Address,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(s.ListenAndServe())
}
