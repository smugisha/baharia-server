package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/smugisha/baharia-server/app/lib"
	"bitbucket.org/smugisha/baharia-server/core/model"
	"bitbucket.org/smugisha/baharia-server/core/repos"
)

func AddTopic(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	u := v.Get("url")
	lib.HubClient.DiscoverAndSubscribe(u, lib.HandleEntry)

	t := model.NewTopic(u)
	repos.Topics.Store(t)

	fmt.Fprintf(w, "Added %s", u)
}
