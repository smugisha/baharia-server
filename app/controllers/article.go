package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/smugisha/baharia-server/core/model"
	"bitbucket.org/smugisha/baharia-server/core/repos"
)

func Articles(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	count, err := strconv.ParseInt(v.Get("count"), 10, 0)
	if err != nil {
		log.Printf("Error: %v\n", err)
		count = 10
	}

	page, err := strconv.ParseInt(v.Get("page"), 10, 0)
	if err != nil {
		log.Printf("Error: %v\n", err)
		page = 1
	}

	articles, err := repos.Articles.FindNewByPage(int(page), int(count))
	if err != nil {
		log.Printf("Error: %v\n", err)
	} else {
		resp, e := articleJSON(articles, 135)
		if e != nil {
			log.Printf("Error: %v\n", e)
			fmt.Fprintf(w, "{error: 1, value: %v}", e)
		} else {
			fmt.Fprintf(w,
				"{error: 0, feed: %s}",
				string(resp))
		}
	}
}

func Like(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	id := v.Get("id")

	article, err := repos.Articles.FindById(id)
	if err != nil {
		log.Printf("Error: %v\n", err)
	} else {
		article.Likes = article.Likes + 1

		e := repos.Articles.UpdateFields(id, map[string]interface{}{
			"Likes": article.Likes,
		})
		if e != nil {
			log.Printf("Error: %v\n", e)
			fmt.Fprintf(w, "{error: 1, value: %v}", e)
		} else {
			fmt.Fprintf(w,
				"{error: 0, value: {id: '%s', likes: %v}}",
				id, article.Likes)
		}
	}
}

func Dislike(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	id := v.Get("id")

	article, err := repos.Articles.FindById(id)
	if err != nil {
		log.Printf("Error: %v\n", err)
	} else {
		article.Likes = article.Likes + 1

		e := repos.Articles.UpdateFields(id, map[string]interface{}{
			"Dislikes": article.Dislikes,
		})
		if e != nil {
			log.Printf("Error: %v\n", e)
			fmt.Fprintf(w, "{error: 1, value: %v}", e)
		} else {
			fmt.Fprintf(w,
				"{error: 0, value: {id: '%s', dislikes: %v}}",
				id, article.Dislikes)
		}
	}
}

func Article(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()
	id := v.Get("id")

	if id == "" {
		fmt.Fprintf(w, "{error: 1, value: %v}", "\"No article in the repo\"")
	} else {
		article, err := repos.Articles.FindById(id)
		if err != nil {
			log.Printf("Error: %v\n", err)
		}

		if article != nil {
			article.Views = article.Views + 1

			e := repos.Articles.UpdateFields(id, map[string]interface{}{
				"Views": article.Views,
			})
			if e != nil {
				log.Printf("Error: %v\n", e)
				fmt.Fprintf(w, "{error: 1, value: %v}", e)
			}

			http.Redirect(w, r, article.Link, 301)
		} else {
			fmt.Fprintf(w, "{ 'error': 1, 'value': 'No article by that id.' }")
		}
	}
}

func articleJSON(articles []model.Article, l int) ([]byte, error) {
	res := make([]interface{}, len(articles))

	for i, a := range articles {
		res[i] = a.OutputStruct(l)
	}

	return json.Marshal(res)
}
