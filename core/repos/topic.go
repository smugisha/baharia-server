package repos

import (
	r "github.com/dancannon/gorethink"

	"bitbucket.org/smugisha/baharia-server/core/infra"
	"bitbucket.org/smugisha/baharia-server/core/model"
)

type TopicRepo struct {
	Table string
}

func NewTopicRepo(t string) TopicRepo {
	return TopicRepo{
		Table: t,
	}
}

func (repo *TopicRepo) FindById(id string) (*model.Topic, error) {
	var topic = new(model.Topic)
	row, err := r.Table(repo.Table).Get(id).Run(infra.RethinkDB())
	if err != nil {
		return topic, err
	}

	if row.IsNil() {
		return nil, nil
	}

	err = row.One(&topic)
	return topic, err
}

func (repo *TopicRepo) FindAll() ([]*model.Topic, error) {
	var topics []*model.Topic
	rows, err := r.Table(repo.Table).Run(infra.RethinkDB())
	if err != nil {
		return topics, err
	}

	err = rows.All(&topics)
	if err != nil {
		return topics, err
	}

	return topics, err
}

func (repo *TopicRepo) Store(topic *model.Topic) error {
	res, err := r.Table(repo.Table).Insert(topic).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	// Find new id of topic if needed
	if topic.Id == "" && len(res.GeneratedKeys) == 1 {
		topic.Id = res.GeneratedKeys[0]
	}

	return nil
}

func (repo *TopicRepo) Update(topic *model.Topic) error {
	_, err := r.Table(repo.Table).Get(topic.Id).Update(topic).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	return nil
}

func (repo *TopicRepo) UpdateFields(id string, fields map[string]interface{}) error {
	_, err := r.Table(repo.Table).Get(id).Update(fields).RunWrite(infra.RethinkDB())

	if err != nil {
		return err
	}

	return nil
}

func (repo *TopicRepo) Delete(id string) error {
	return r.Table(repo.Table).Get(id).Delete().Exec(infra.RethinkDB())
}

func (repo *TopicRepo) DeleteAll() error {
	return r.Table(repo.Table).Delete().Exec(infra.RethinkDB())
}
