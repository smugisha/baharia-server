package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

const (
	Dev  string = "development"
	Prod string = "production"
	Test string = "testing"
)

type Config struct {
	Name    string `yaml:"name"`
	Env     string `yaml:"env"`
	Address string `yaml:"address"`
	Host    string `yaml:"host"`

	RethinkDB RethinkDB `yaml:"rethinkdb"`

	Tables []Table `yaml:"tables"`
	Hub    PubHub  `yaml:"hub"`

	ApiCredentials map[string]Api `yaml:"api"`
}

type RethinkDB struct {
	MaxIdle  int    `yaml:"max_idle"`
	Address  string `yaml:"address"`
	Database string `yaml:"database"`
}

type Table struct {
	Name    string   `yaml:"name"`
	Indexes []string `yaml:"indexes"`
}

type PubHub struct {
	Host    string `yaml:"host"`
	Port    string `yaml:"port"`
	Watcher string `yaml:"watcher"`
}

type Api struct {
	Key    string `yaml:"key"`
	Secret string `yaml:"secret"`
}

func NewConfig() *Config {
	return &Config{
		Env:     Dev,
		Address: ":3000",
		RethinkDB: RethinkDB{
			MaxIdle: 10,
		},
	}
}

func LoadFile(conf *Config, file string) error {
	conf_data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(conf_data, conf)
	if err != nil {
		return err
	}

	return nil
}
