package data

import (
	"bitbucket.org/smugisha/baharia-server/core/config"
)

func Setup(conf *config.Config, exampleData bool) {
	setupRethinkDB(conf, exampleData)
}
