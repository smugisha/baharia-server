package data

import (
	r "github.com/dancannon/gorethink"

	"bitbucket.org/smugisha/baharia-server/core/config"
	"bitbucket.org/smugisha/baharia-server/core/infra"
	"bitbucket.org/smugisha/baharia-server/core/model"
	"bitbucket.org/smugisha/baharia-server/core/repos"
)

func setupRethinkDB(conf *config.Config, exampleData bool) {
	createDatabase(conf)
	createTables(conf)

	if exampleData {
		createTopics()
	}
}

func createDatabase(conf *config.Config) {
	r.DBDrop(conf.RethinkDB.Database).RunWrite(infra.RethinkDB())
	r.DBCreate(conf.RethinkDB.Database).RunWrite(infra.RethinkDB())
}

func createTables(conf *config.Config) {
	db := conf.RethinkDB.Database

	for _, t := range conf.Tables {
		r.DB(db).TableDrop(t.Name).RunWrite(infra.RethinkDB())
		r.DB(db).TableCreate(t.Name).RunWrite(infra.RethinkDB())

		createIndexes(db, t.Name, t.Indexes)
	}
}

func createIndexes(db, t string, indexes []string) {
	for _, col := range indexes {
		r.DB(db).Table(t).IndexCreate(col).RunWrite(infra.RethinkDB())
	}
}

func createTopics() {
	topicLinks := []string{
		"http://issamichuzi.blogspot.com/feeds/posts/default/",
		"http://wanamuzikiwatanzania.blogspot.com/feeds/posts/default/",
		"http://kingkapita.com/feeds/posts/default/",
 		"http://feeds.feedburner.com/Hassbabysmapacha",
		"http://tembeatz.blogspot.com/feeds/posts/default/",
		"http://hassbabytz.com/feeds/posts/default/",
		"http://ilovetanzania.blogspot.com/feeds/posts/default",
		"http://kajunason.com/feeds/posts/default",
		"http://tutokemedia.blogspot.com/feeds/posts/default",
		"http://harusini.blogspot.com/feeds/posts/default",
		"http://wananjenje.blogspot.com/feeds/posts/default",
		"http://cmeognam.blogspot.com/feeds/posts/default/",
        "https://medium.com/feed/latest",
        "https://www.reddit.com/.rss",
        "https://www.reddit.com/r/all/.rss",
	}

	for _, link := range topicLinks {
		topic := model.NewTopic(link)

		err := repos.Topics.Store(topic)
		if err != nil {
			//
		}
	}
}
