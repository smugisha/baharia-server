package model

type User struct {
	Phone    string
	DigitsId string
	Id       string `gorethink:"id,omitempty"`
}

func NewUser(digitsId, phone string) *User {
	return &User{
		Phone:    phone,
		DigitsId: digitsId,
	}
}
