package model

type Topic struct {
	Name   string
	Author string
	Link   string
	Id     string `gorethink:"id,omitempty"`
}

func NewTopic(link string) *Topic {
	return &Topic{
		Link: link,
	}
}
