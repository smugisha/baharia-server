package model

type Thumbnail struct {
	Url    string
	Height string
	Width  string
}

type Article struct {
	Title     string
	Author    string
	Link      string
	Thumbnail Thumbnail
	TopImage  Thumbnail
	Content   string
	Intro     string
	Published string
	Views     uint
	Likes     uint
	Dislikes  uint

	Id string `gorethink:"id,omitempty"`
}

func NewArticle(title, author, published string) *Article {
	return &Article{
		Title:     title,
		Author:    author,
		Published: published,
		Views:     0,
		Likes:     0,
		Dislikes:  0,
	}
}

func (a *Article) previewIntro(l int) string {
	r := []rune(a.Intro)
	if l < len(r) {
		return string(r[0:l])
	} else {
		return string(r)
	}
}

func (a *Article) OutputStruct(l int) interface{} {
	type ArticleResponse struct {
		Id string

		Title     string
		Author    string
		Link      string
		Published string

		TopImage string
		Intro    string
		Content  string

		Views uint
		Likes uint
	}

	return ArticleResponse{
		Id: a.Id,

		Title:     a.Title,
		Author:    a.Author,
		Link:      a.Link,
		Published: a.Published,

		TopImage: a.TopImage.Url,
		Intro:    a.previewIntro(l),
		Content:  a.Content,

		Views: a.Views,
		Likes: a.Likes,
	}
}
