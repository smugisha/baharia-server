package infra

import (
	"os"

	"bitbucket.org/smugisha/baharia-server/core/config"

	r "github.com/dancannon/gorethink"
)

var (
	rdb_session *r.Session
)

func InitRethinkDB(conf config.RethinkDB) {
	var err error

	address := conf.Address

	// Check for environment variables
	envHost := os.Getenv("RETHINKDB_HOST")
	envPort := os.Getenv("RETHINKDB_PORT")

	if envHost != "" && envPort != "" {
		address = envHost + ":" + envPort
	}

	rdb_session, err = r.Connect(r.ConnectOpts{
		Address:  address,
		Database: conf.Database,
	})

	if err != nil {
		panic(err)
	}
}

func RethinkDB() *r.Session {
	return rdb_session
}
